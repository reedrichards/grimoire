#+TITLE: i3 config file (v4)

I have a lot of custom keyboard shortcuts that I use with [[https://i3wm.org/][i3wm]] and Its becoming hard to remember all of them. So in an effort be a bit more organized with my configuration, I decided to add some docs.



Please see https://i3wm.org/docs/userguide.html for a complete reference!

compiled using [[https://orgmode.org/manual/Extracting-Source-Code.html#Extracting-Source-Code][org-babel]]
#+begin_src elisp
(org-babel-tangle)
#+end_src
or ~, b t~

create symlink to ~config~ in current working directory
#+begin_src bash
ln -s config.conf config
#+end_src

~stow~ i3
#+begin_src bash
stow i3
#+end_src

* modifier keys
  #+begin_src conf :tangle yes
set $mod Mod4
# hyper
set $modH Mod3
set $modSR Mod5
  #+end_src
* font
#+begin_src conf :tangle yes
# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
# font pango:monospace 8
# font pango:DroidSansMono 24
font pango:SourceCodePro 24

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
# font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.
#+end_src

* Use Mouse+$mod to drag floating windows to their wanted position
  #+begin_src conf :tangle yes
# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod
  #+end_src
* custom keybindings
** modes
*** resize
    bound inside of ~r mode~
#+begin_src conf :tangle yes
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Caps_Lock mode "default"
}


#+end_src
*** re
    allows to resize, and rename buffers or windows
#+begin_src conf :tangle yes
set $re Launch: re[s]ize re[n]ame
bindsym $mod+r mode "$re"
mode "$re" {

        bindsym s mode "resize"
        bindsym n exec i3-input -F 'rename workspace to "%s"' -P 'New number and name for this workspace: '
        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Caps_Lock mode "default"
}
#+end_src
**** prefix ~r~
**** commands
***** ~s~
      enter ~resize~ mode
***** ~n~
      rename workspace
#+begin_quote
!IMPORTANT

Don't forget to add number
#+end_quote
** restart and exit
#+begin_src conf :tangle yes
# reload the configuration file
# bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"


#+end_src
** start a terminal
   #+begin_src conf :tangle yes
bindsym $mod+Return exec alacritty
   #+end_src
** attach to tmux primary session
   doesn't work on first run
   #+begin_src conf :tangle yes
bindsym $mod+Shift+Return exec ~/bin/alacritty-psesh
   #+end_src
*** ~~/bin/alacritty-psesh~
#+begin_src bash :tangle no
#!/bin/bash

alacritty -e "/home/rob/bin/attach-psesh"

#+end_src
*** ~/home/rob/bin/attach-psesh~
    #+begin_src bash :tangle no
#!/bin/bash
## spawn a zsh process attached to promary session
/usr/bin/zsh --login -i -c "/usr/bin/tmux -u -2 new-session -Ad -s main"
    #+end_src
** open field notes file for today
   #+begin_src conf :tangle yes
bindsym $mod+Shift+f exec field-notes
   #+end_src
*** ~field-notes~
#+begin_src bash :tangle no
#!/bin/bash
DATE=`date +%Y-%m-%d`
emacsclient -c ~/documents/field-notes/$DATE.org
#+end_src

** lock screen
   #+begin_src conf :tangle yes
bindsym $modH+l exec lock.sh
   #+end_src

** current date to clipboard
   #+begin_src conf :tangle yes
bindsym $modH+t exec date | xclip
   #+end_src
   example output: ~Fri 27 Dec 2019 09:27:35 AM MST~

** open browser [0/1]
*** ❎ make this modal
    - State "❎"         from              [2019-12-27 Fri 09:31]
   #+begin_src conf :tangle yes
bindsym $mod+b exec chromium
bindsym $mod+Shift+g exec xdg-open "https://gitlab.com"
bindsym $modH+g exec xdg-open "https://grimeywebsites.com"
bindsym $mod+i exec chromium --app=https://irc.grimeywebsites.com
bindsym $mod+m exec chromium --app=https://messages.android.com
bindsym $mod+Shift+c exec chromium --app=https://calendar.google.com
# # text messages
# exec_always chromium --app=https://messages.android.com
   #+end_src


** brightness [0/1]
*** ❎ make this modal
     - State "❎"         from              [2019-12-23 Mon 16:04]
#+begin_src conf :tangle yes
#dim
bindsym $modH+d exec xrandr --output HDMI-1 --brightness .3 & xrandr --output DP-1 --brightness .3 & xrandr --output HDMI-1-2 --brightness .3

bindsym $mod+shift+d exec xrandr --output HDMI-1 --brightness .5 & xrandr --output DP-1 --brightness .5 & xrandr --output HDMI-1-2 --brightness .5
#brighten
bindsym $modH+Shift+d exec xrandr --output HDMI-1 --brightness 1 & xrandr --output DP-1 --brightness 1 & xrandr --output HDMI-1-2 --brightness 1
#+end_src
** open port in localhost [0/1]
*** ❎ select defaults
    - State "❎"         from              [2019-12-23 Mon 16:06]
   #+begin_src conf :tangle yes
bindsym $modH+Shift+l exec i3-input -F 'exec localhost-open  %s && i3-msg workspace number 1'  -P 'port: '
   #+end_src

** prompt for a password
   #+begin_src conf :tangle yes
bindsym $modH+p exec i3-input -F 'exec md5c %s '  -P 'pass: '
   #+end_src

** google search
   #+begin_src conf :tangle yes
# bindsym $mod+s exec i3-input -F 'exec ~/bin/google-search  %s & i3-msg workspace number 1'  -P 'search: '
# bindsym $mod+s exec xdg-open https://searx.grimeywebsites.com & i3-msg workspace number 1
bindsym $mod+s exec xdg-open https://google.com & i3-msg workspace number 1
bindsym $mod+Shift+s exec google-search  & i3-msg workspace number 1
# bindsym $mod+Shift+s exec i3-input -F 'exec ~/bin/google-search-lucky  %s && i3-msg workspace 1'  -P 'search: '
   #+end_src

** connect to slack
   #+begin_src conf :tangle yes
bindsym $modH+Shift+s exec slack
   #+end_src

** volume
   #+begin_src conf :tangle yes
bindsym $mod+Shift+v exec pavucontrol
   #+end_src

** projectM
   #+begin_src conf :tangle yes
bindsym $mod+Shift+p exec projectM-pulseaudio
   #+end_src

** screenshot
   #+begin_src conf :tangle yes
bindsym --release $mod+x exec scrot -s
   #+end_src

** execute clipboard in shell
   #+begin_src conf :tangle yes
bindsym $mod+p exec execute-clipboard
   #+end_src


# emacs stuff
** connect to emacs daemon
   #+begin_src conf :tangle yes
bindsym $mod+c exec emacsclient -c
   #+end_src

** git clone
   #+begin_src conf :tangle yes
bindsym $mod+g exec git clone "$(xclip -o)"
   #+end_src

** kill focused window
   #+begin_src conf :tangle yes
bindsym $mod+Shift+q kill
   #+end_src

** start dmenu (a program launcher)
   #+begin_src conf :tangle yes
bindsym $mod+d exec rofi -show combi
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
#bindsym $mod+d exec --no-startup-id i3-dmenu-desktop
   #+end_src
** media keybinds
   #+begin_src conf :tangle yes
bindsym XF86MonBrightnessUp exec xrandr --output eDP-1 --brightness 1.0
bindsym XF86MonBrightnessDown exec xrandr --output eDP-1 --brightness 0.5
   #+end_src

# Multimedia Keys
*** increase volume
    #+begin_src conf :tangle yes
bindsym XF86AudioRaiseVolume exec amixer -D pulse sset Master 5%+
    #+end_src
 # amixer -q set Master 5%+ unmute
*** decrease volume
    #+begin_src conf :tangle yes
bindsym XF86AudioLowerVolume exec exec amixer -D pulse sset Master 5%-
    #+end_src
 # amixer -q set Master 5%- unmute
*** mute volume
    #+begin_src conf :tangle yes
bindsym XF86AudioMute exec amixer -q set Master mute
    #+end_src
*** pause / play / next / previous
    #+begin_src conf :tangle yes
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous
    #+end_src
* Navigation

** change focus with vim keybindings
   #+begin_src conf :tangle yes
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right
   #+end_src

*** alternatively, you can use the cursor keys:
   #+begin_src conf :tangle yes
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right
   #+end_src

** move focused window with vim keybindings
   #+begin_src conf :tangle yes
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right
   #+end_src

*** alternatively, you can use the cursor keys:
   #+begin_src conf :tangle yes
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right
   #+end_src

** split in horizontal orientation
   #+begin_src conf :tangle yes
bindsym $mod+Shift+semicolon split h
   #+end_src

** split in vertical orientation
   #+begin_src conf :tangle yes
bindsym $mod+v split v
   #+end_src

** enter fullscreen mode for the focused container
   #+begin_src conf :tangle yes
bindsym $mod+f fullscreen toggle
   #+end_src

** change container layout (stacked, tabbed, toggle split)
   #+begin_src conf :tangle yes
# bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split
   #+end_src

** toggle tiling / floating
   #+begin_src conf :tangle yes
bindsym $mod+Shift+space floating toggle
   #+end_src

** change focus between tiling / floating windows
   #+begin_src conf :tangle yes
bindsym $mod+space focus mode_toggle
   #+end_src

** focus the parent container
   #+begin_src conf :tangle yes
bindsym $mod+a exec /home/rob/.emacs_anywhere/bin/run
   #+end_src

** focus the child container
   #+begin_src conf :tangle yes
#bindsym $mod+d focus child
   #+end_src

** multi monitor
   #+begin_src conf :tangle yes
bindsym $mod+1 workspace number 1; move workspace to output HDMI-0
bindsym $mod+2 workspace number 2; move workspace to output HDMI-0
bindsym $mod+3 workspace number 3; move workspace to output HDMI-0
bindsym $mod+4 workspace number 4; move workspace to output HDMI-0
bindsym $mod+5 workspace number 5; move workspace to output HDMI-0
bindsym $mod+6 workspace number 6; move workspace to output HDMI-0
bindsym $mod+7 workspace number 7; move workspace to output HDMI-0
bindsym $mod+8 workspace number 8; move workspace to output HDMI-0
bindsym $mod+9 workspace number 9; move workspace to output HDMI-0
bindsym $mod+0 workspace number 10; move workspace to output HDMI-0

bindsym $modH+1 workspace number 11; move workspace to output eDP-1-1
bindsym $modH+2 workspace number 12; move workspace to output eDP-1-1
bindsym $modH+3 workspace number 13; move workspace to output eDP-1-1
bindsym $modH+4 workspace number 14; move workspace to output eDP-1-1
bindsym $modH+5 workspace number 15; move workspace to output eDP-1-1
bindsym $modH+6 workspace number 16; move workspace to output eDP-1-1
bindsym $modH+7 workspace number 17; move workspace to output eDP-1-1
bindsym $modH+8 workspace number 18; move workspace to output eDP-1-1
bindsym $modH+9 workspace number 19; move workspace to output eDP-1-1
bindsym $modH+0 workspace number 20; move workspace to output eDP-1-1

bindsym $modSR+1 workspace number 21; move workspace to output HDMI-1-2
bindsym $modSR+2 workspace number 22; move workspace to output HDMI-1-2
bindsym $modSR+3 workspace number 23; move workspace to output HDMI-1-2
bindsym $modSR+4 workspace number 24; move workspace to output HDMI-1-2
bindsym $modSR+5 work number 25; move workspace to output HDMI-1-2
bindsym $modSR+6 workspace number 26; move workspace to output HDMI-1-2
bindsym $modSR+7 workspace number 27; move workspace to output HDMI-1-2
bindsym $modSR+8 workspace number 28; move workspace to output HDMI-1-2
bindsym $modSR+9 workspace number 29; move workspace to output HDMI-1-2
bindsym $modSR+0 workspace number 30; move workspace to output HDMI-1-2

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10

bindsym $modH+Shift+1 move container to workspace number 11
bindsym $modH+Shift+2 move container to workspace number 12
bindsym $modH+Shift+3 move container to workspace number 13
bindsym $modH+Shift+4 move container to workspace number 14
bindsym $modH+Shift+5 move container to workspace number 15
bindsym $modH+Shift+6 move container to workspace number 16
bindsym $modH+Shift+7 move container to workspace number 17
bindsym $modH+Shift+8 move container to workspace number 18
bindsym $modH+Shift+9 move container to workspace number 19
bindsym $modH+Shift+0 move container to workspace number 20

bindsym $modSR+Shift+1 move container to workspace number 21
bindsym $modSR+Shift+2 move container to workspace number 22
bindsym $modSR+Shift+3 move container to workspace number 23
bindsym $modSR+Shift+4 move container to workspace number 24
bindsym $modSR+Shift+5 move container to workspace number 25
bindsym $modSR+Shift+6 move container to workspace number 26
bindsym $modSR+Shift+7 move container to workspace number 27
bindsym $modSR+Shift+8 move container to workspace number 28
bindsym $modSR+Shift+9 move container to workspace number 29
bindsym $modSR+Shift+0 move container to workspace number 30
   #+end_src

* Cosmetics

  #+begin_src conf :tangle yes
# class                 border  backgr. text    indicator child_border
client.focused          #003300 #003300 #ffffff #003300   #003300
client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #333333 #222222 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff
  #+end_src

** i3bar
   #+begin_src conf :tangle yes
# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3status
        position top
        tray_padding 0
        separator_symbol "x"
    colors {
        background #000000
        statusline #ffffff
        separator #666666

        focused_workspace  #003300 #003300 #ffffff
        active_workspace   #333333 #5f676a #ffffff
        inactive_workspace #333333 #222222 #888888
        urgent_workspace   #2f343a #900000 #ffffff
        binding_mode       #2f343a #900000 #ffffff
    }
}

   #+end_src

** gaps
   #+begin_src conf :tangle yes
gaps inner 15
gaps outer 0
for_window [class=".*"] border pixel 0
# hide_edge_borders both
   #+end_src

* scratchpad

** Make the currently focused window a scratchpad
   #+begin_src conf :tangle yes
bindsym $mod+Shift+minus move scratchpad
   #+end_src

** Show the first scratchpad window
   #+begin_src conf :tangle yes
bindsym $mod+minus scratchpad show
   #+end_src

* startup scripts
# [class="Emacs"] move window to workspace 1
# [class="Alacritty"] move window to workspace 1
** notifications
   #+begin_src conf :tangle yes
exec --no-startup-id dunst
   #+end_src
** wallpaper
   #+begin_src conf :tangle yes
exec --no-startup-id nitrogen --restore
   #+end_src
** faster typing
   #+begin_src conf :tangle yes
exec --no-startup-id xset r rate 250 60
   #+end_src
** desktop layout
   #+begin_src conf :tangle yes
exec --no-startup-id $HOME/.screenlayout/desktop.sh
# exec --no-startup-id $HOME/.screenlayout/mobile.sh
   #+end_src
** redshift
   #+begin_src conf :tangle yes
# exec --no-startup-id redshift-gtk
   #+end_src
** compton
   #+begin_src conf :tangle yes
exec --no-startup-id compton &
   #+end_src
** polybar
   #+begin_src conf :tangle yes
# bar
# exec_always --no-startup-id $HOME/.config/polybar/launch-polybar.sh
   #+end_src
** custom keymappings
#+begin_src conf :tangle yes
exec_always --no-startup-id xmodmap $HOME/.Xmodmap
exec_always --no-startup-id /usr/bin/setxkbmap -option "caps:swapescape"
#+end_src
** flashfocus
#+begin_src conf :tangle yes
exec_always --no-startup-id flashfocus
bindsym $mod+n exec --no-startup-id flash_window
#+end_src
