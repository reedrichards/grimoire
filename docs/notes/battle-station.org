* goals
  drew a lot of inspiration from [[https://www.reddit.com/r/battlestations/comments/8bqfoa/screen_real_estate/][this reddit post]].  Pic of setup:
 [[https://i.imgur.com/UB4Da2g.jpg][src]]
 [[https://imgur.com/a/0YeoO][wallpaper]]

with modifications made to the desk, keyboard, and
** [[https://www.rtings.com/tv/reviews/sony/x900e][Main Monitor: 49" 4K Sony X900E TV]] =$1000=
** [[http://www.dell.com/ed/business/p/dell-p2717h-monitor/pd][Sides(2): 27" Dell P2717H]] =$514=
** [[https://www.amazon.com/WALI-Universal-Monitor-Adjustable-Capacity/dp/B01NBYJ6X6/ref=sr_1_4?ie=UTF8&qid=1523550759&sr=8-4&keywords=wali+wall+mount+monitor][Wall Mounted with two of these]] =$35=
** [[https://ergodox-ez.myshopify.com/11523264/checkouts/49feff7f26efe8fd79f307b8b9638bd5][Keyboard: ergodox]] =$325=
** [[https://www.logitech.com/en-us/product/mx-master-2s-flow][Mouse: Logitech MX Master 2]] =$100=
** [[https://www.hermanmiller.com/products/seating/office-chairs/aeron-chairs/][Chair: Herman Miller Aeron]] =$1100=
- [[https://www.amazon.com/gp/offer-listing/B07FCPGFC5/ref=dp_olp_new_mbc?ie=UTF8&condition=new][with these wheels]] =$72=
** [[https://www.amazon.com/ApexDesk-2-Button-Electric-Adjustable-Espresso/dp/B016R3VWNM/ref=sr_1_1?qid=1557620613&refinements=p_n_location_browse-bin%3A2725009011&s=office-products&sr=1-1][ApexDesk VT60ESS-S Vortex Series 60" 2-Button Electric Height Adjustable Sit to Stand Desk, Espresso Top with Standard Controller]] =$456=
** current estimated total cost =$2612=

*** notes from original inspiration on reddit


*** Microphone Ideas
    :PROPERTIES:
    :CUSTOM_ID: microphone-ideas
    :END:

 I'm not super familiar with Audio lingo, but I'm pretty sure I'm looking
 for a cardiod Microphone Pickup/Polar pattern.

 Not really sure the difference between these two other than price, and
 size, but the e904 could possibly be clipped to my monitor

**** [[https://en-us.sennheiser.com/instrument-microphone-drums-percussion-cardioid-dynamic-e-904][Sennheiser e 904]] =169.95=

**** [[https://en-us.sennheiser.com/condenser-microphone-studio-recordings-professional-mk-4][Sennheiser MK 4]] =229.95=

 would need to get stand
 [[https://en-us.sennheiser.com/mks-4][Sennheiser MKS 4]] =99.95=
