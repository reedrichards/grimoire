#+TITLE: Kubernetes

* Baremetal at home setup- the =intitial= step should be configured on all nodes.

- If you are installing these on a home network, configure static ip
  addresses for your nodes so that your router doesn't reassign them.

- You're going to want at least 2cpu cores and 4gb's of ram on your
  master node
** initial
Install Ubuntu 16.04 LTS. Kubernetes is developed on 16.04, and has the
version of docker supported for kubernetes in its apt repositories.

#+BEGIN_QUOTE
  Note: you want to do this step on all nodes in your cluster before
  continuing
#+END_QUOTE

Disable swap then edit your fstab removing any entry for swap
partitions.

#+BEGIN_SRC sh
  sudo swapoff -a
  sudo vi /etc/fstab
   sudo reboot
#+END_SRC

install curl

#+BEGIN_SRC sh
  # just in case
  sudo apt install curl -y
#+END_SRC

Add Google's apt repository gpg key

#+BEGIN_SRC sh
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#+END_SRC

Add the Kubernetes apt repository

#+BEGIN_SRC sh
sudo bash -c 'cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF'
#+END_SRC

Update the package list

#+BEGIN_EXAMPLE
  sudo apt-get update
#+END_EXAMPLE

Install the required packages

#+BEGIN_SRC sh
  sudo apt-get install -y kubelet kubeadm kubectl docker.io
#+END_SRC

docker permissions

#+BEGIN_SRC sh
  sudo usermod $USER -aG docker
  newgrp docker
#+END_SRC

we want kuberentes to handle updates, not the os

#+BEGIN_SRC sh
  sudo apt-mark hold kubelet kubeadm kubectl docker.io
#+END_SRC

#+BEGIN_QUOTE
  Note: k8s doesn't play well with newer versions of docker, so
  installing docker.io makes sure that you won't have any issues
#+END_QUOTE

check to see if docker is installed proper

#+BEGIN_SRC sh
  docker version
#+END_SRC
** master node
Create our kubernetes cluster, specifying a pod network range, and a
public ip address

#+BEGIN_SRC sh
  sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --apiserver-cert-extra-sans=$PUBLICIP
#+END_SRC

Configure our account on the master to have admin access to the API
server from a non-privileged account.

#+BEGIN_SRC sh
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
#+END_SRC

Apply Weavenet RBAC

#+BEGIN_SRC sh
  kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
#+END_SRC

Look for the all the system pods and weavenet pod to change to Running.
The DNS pod won't start until the Pod network is deployed and Running.

#+BEGIN_SRC sh
  kubectl get pods --all-namespaces
#+END_SRC

Get a list of our current nodes, just the master.

#+BEGIN_SRC sh
  kubectl get nodes
#+END_SRC
** worker node
If you didn't keep the output, on the master, you can get the token.

#+BEGIN_SRC sh
  # on the master
  sudo kubeadm token list
#+END_SRC

If you need to generate a new token, perhaps the old one timed
out/expired.

#+BEGIN_SRC sh
  #on the master
  sudo kubeadm token create
#+END_SRC

On the master, you can find the ca cert hash.

#+BEGIN_SRC sh
  openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'
#+END_SRC

Using the master (API Server) IP address or name, the token and the cert
has, let's join this Node to our cluster.

#+BEGIN_SRC sh
  sudo  kubeadm join 192.168.1.190:6443 --token syvrch.ca1qtym4po3rtxbv --discovery-token-ca-cert-hash sha256:f7c56dbcf2e61c38c2ae0936722195dad2c1851214242lkjflkdsjflksdjflsl

  sudo  kubeadm join $PUBLICIP:6443 --token syvrch.ca1qtym4po3rtxbv --discovery-token-ca-cert-hash sha256:f7c56dbcf2e61c38c2ae0936722195dad2c18512dofisl14242ba4dd8cd119a3deba
#+END_SRC
** cleanup
reset kubernetes

#+BEGIN_SRC sh
  sudo kubeadm reset
#+END_SRC

clean up docker

#+BEGIN_SRC sh

  docker rm -f $(docker ps -qa)
  docker volume rm $(docker volume ls -q)
#+END_SRC

clean kubernetes dirs, but actually don't run this command

#+BEGIN_SRC sh
  cleanupdirs="/var/lib/etcd /etc/kubernetes /etc/cni /opt/cni /var/lib/cni /var/run/calico /opt/rke"
  for dir in $cleanupdirs; do
      echo "Removing $dir"
      rm -rf $dir
  done
#+END_SRC

remove .kube if necessary =bash sudo rm -rf ~/.kube=

clean up routing tables

#+BEGIN_SRC sh
  sudo iptables -F
  sudo iptables -t nat -F
  sudo iptables -t mangle -F
  sudo iptables -X
#+END_SRC
** deployments
Imperatively working with your cluster. Run will "generate" a Deployment by default. This is pulling a specified image from Google's container registry. kubectl run, will convert a pod creation into a "Deployment generation"
 http://kubernetes.io/docs/user-guide/kubectl-conventions/#generators


#+BEGIN_SRC bash
kubectl run hello-world --image=gcr.io/google-samples/hello-app:1.0
#+END_SRC

But let's deploy a single pod too

#+BEGIN_SRC bash
kubectl run hello-world-pod --image=gcr.io/google-samples/hello-app:1.0 --generator=run-pod/v1
#+END_SRC

Expose the Deployment as a Serivce.

This will create a Service for the ReplicaSet behind the Deployment We are exposing our serivce on port 80, connecting to an application running on 8080 in our pod. Port: Interal Cluster Port, the Service's port. You will point cluster resources here. TargetPort: The Pod's Serivce Port, your application. That one we defined when we started the pods.

#+BEGIN_SRC bash
kubectl expose deployment hello-world --port=80 --target-port=8080 --nodePort=30001
#+END_SRC

export resources  Exported resources are stripped of cluster-specific information.

#+BEGIN_SRC bash
kubectl get deployment hello-world -o yaml --export > deployment-hello-world.yaml
#+END_SRC

update our configuration with apply

#+BEGIN_SRC bash
kubectl apply -f deployment-hello-world.yaml
#+END_SRC
* 👷 Exposing Deployments on Bare Metal K8's clusters sanely [0/1]
  - State "👷"         from              [2020-04-03 Fri 23:36]
  loosely based on https://blog.inkubate.io/generate-automatically-ssl-certificates-for-your-kubernetes-services/
** [[https://gitlab.com/reedrichards-infra/metalb][Load balancer]] 
*** [[https://metallb.universe.tf/installation/][Installation]] 

    enable stric ARP mode

    #+BEGIN_SRC bash
kubectl edit configmap -n kube-system kube-proxy
    
    #+END_SRC
    and set 
#+BEGIN_SRC yaml
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
mode: "ipvs"
ipvs:
  strictARP: true


#+END_SRC

  You can also add this configuration snippet to your kubeadm-config, 
 just append it with ~---~ after the main configuration.

**** Installation By Manifest 
     #+BEGIN_SRC bash
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.9.3/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.9.3/manifests/metallb.yaml
# On first install only
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
   
     #+END_SRC
**** Create Layer2 Config 
  gives MetalLB control over IPs from 192.168.1.240 to 192.168.1.250, 
  and configures Layer 2 mode
  #+BEGIN_SRC yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 192.168.1.2-192.168.1.2555


  #+END_SRC
**** Deploy nginx with new load balancer
     #+BEGIN_SRC yaml
apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1
        ports:
        - name: http
          containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
  loadBalancerIP: 192.168.1.1
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx
  type: LoadBalancer
     #+END_SRC
** 👷 [[https://gitlab.com/reedrichards-infra/ingress][Ingress]] 
   - State "👷"         from              [2020-04-03 Fri 23:33]
*** ☑ [[https://cert-manager.io/docs/installation/kubernetes/][Installation]] 
    CLOSED: [2020-04-03 Fri 23:39]
    - State "☑"          from              [2020-04-03 Fri 23:39]
    #+BEGIN_SRC bash
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.1/cert-manager.yaml

    #+END_SRC
**** verify with
#+BEGIN_SRC bash
kubectl get pods --namespace cert-manager

#+END_SRC

*** Configure Cluster Issuer
    cloudflare stuff irellevant rn because i can't serve from my 
apartment atm
***** Add site to cloudflare
      replace nameservers of your domain with cloudflare nameservers
****** [[https://dash.cloudflare.com/][Cloudflare Dashboard]] 
****** [[https://ap.www.namecheap.com/domains/list/][namecheap domain list]] 
***** Confugre router
****** Install [[https://openwrt.org/docs/guide-quick-start/factory_installation][openwrt]]  on router
****** Install [[https://github.com/openwrt/packages/blob/openwrt-18.06/net/unbound/files/README.md][unbound]]  dns on openwrt
******* configure [[/ssh:root@192.168.1.1:/etc/unbound/unbound.conf][unbound.conf]] 
	#+BEGIN_SRC conf
server:
        # ...
	# whitespace is not necessary, but looks cleaner.
	
	# k8s ingress
        local-zone: "jjk.is" redirect
        local-data: "jjk.is 864000 in A 192.168.1.2"

	
	#+END_SRC
******* restart unbound
#+BEGIN_SRC bash
ssh root@192.168.1.1 /etc/init.d/unbound restart

#+END_SRC

#+RESULTS:

***** [[https://cert-manager.io/docs/configuration/acme/][Cert Manager Acme Config Docs]] 
      #+BEGIN_SRC yaml
apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
spec:
  acme:
    ...
    solvers:
    - dns01:
        cloudflare:
          email: user@example.com
          apiKeySecretRef:
            name: cloudflare-apikey-secret
            key: apikey
      selector:
        dnsNames:
        - 'example.com'
        - '*.example.com'
      
      #+END_SRC
***** [[https://cert-manager.io/docs/configuration/acme/dns01/cloudflare/#api-tokens][Cet Manager Cloudflare API Token Issuer]] 
Tokens can be created at *User Profile > API Tokens > API Tokens*. 
The following settings are recommended:

Permissions:
 - Zone - DNS - Edit
 - Zone - Zone - Read

Zone Resources:
 - Include - All Zones
** [[https://www.terraform.io/docs/providers/cloudflare/index.html][cloudflare terraform]] could be useful for automating some of these steps
* auto completion
  Ok, so now that we're tired of typing commands out, let's enable bash
auto-complete of our kubectl commands

#+BEGIN_SRC sh
  sudo apt-get install bash-completion
  echo "source <(kubectl completion bash)" >> ~/.bashrc
  echo "alias k='kubectl'" >> ~/.bashrc
  source ~/.bashrc
  complete -F __start_kubectl k
#+END_SRC
* Resource Definitions
** Namespace
   :PROPERTIES:
   :CUSTOM_ID: namespace
   :END:

   #+BEGIN_SRC yaml :tangle ./kubernetes/example-namespace.yaml
  apiVersion: v1
  kind: Namespace
  metadata:
    name: lobsters
   #+END_SRC

** PersistentVolume
   :PROPERTIES:
   :CUSTOM_ID: persistentvolume
   :END:

#+BEGIN_EXAMPLE
  apiVersion: v1
  kind: PersistentVolume
  metadata:
    name: lobsters
    namespace: lobsters
  spec:
    capacity:
      storage: 20Gi
    # volumeMode field requires BlockVolume Alpha feature gate to be enabled.
    volumeMode: Filesystem
    accessModes:
    - ReadWriteMany
    persistentVolumeReclaimPolicy: Retain
    local:
      path: /mnt/lobsters
    nodeAffinity:
      required:
        nodeSelectorTerms:
        - matchExpressions:
          - key: kubernetes.io/hostname
            operator: In
            values:
            - tom
#+END_EXAMPLE

** PersistentVolumeClaim
   :PROPERTIES:
   :CUSTOM_ID: persistentvolumeclaim
   :END:

#+BEGIN_EXAMPLE
  apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: lobsters-mysql-claim
    namespace: lobsters
    labels:
      app: lobsters
  spec:
    accessModes:
      - ReadWriteMany
    resources:
      requests:
        storage: 20Gi
#+END_EXAMPLE

** Deployment
   :PROPERTIES:
   :CUSTOM_ID: deployment
   :END:

#+BEGIN_EXAMPLE
  apiVersion: extensions/v1beta1
  kind: Deployment
  metadata:
    labels:
      app: mysql
    name: mysql
    namespace: lobsters
  spec:
    replicas: 1
    template:
      metadata:
        labels:
          app: mysql
        name: mysql
      spec:
        containers:
          - name: mysql
            image: mysql:5.6.32
            env:
              - name: MYSQL_ROOT_PASSWORD
                valueFrom:
                  secretKeyRef:
                    name: lobsters
                    key: root-password
              - name: MYSQL_PASSWORD
                valueFrom:
                  secretKeyRef:
                    name: lobsters
                    key: mysql-password
              - name: MYSQL_USER
                value: lobsters
              - name: MYSQL_DATABASE
                value: lobsters
            ports:
              - name: mysql
                containerPort: 3306
            volumeMounts:
              - mountPath: /var/lib/mysql
                name: mysql
        volumes:
          - name: "mysql"
            persistentVolumeClaim:
              claimName: lobsters-mysql-claim
#+END_EXAMPLE

** Service
   :PROPERTIES:
   :CUSTOM_ID: service
   :END:

#+BEGIN_EXAMPLE
  apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: lobsters
    name: lobsters
    namespace: lobsters
  spec:
    ports:
    - port: 3000
      protocol: TCP
      targetPort: 3000
      nodePort: 30003
    selector:
      app: lobsters
    type: LoadBalancer
#+END_EXAMPLE

** CronJob with hostPath volume
   :PROPERTIES:
   :CUSTOM_ID: cronjob-with-hostpath-volume
   :END:

#+BEGIN_EXAMPLE
  apiVersion: batch/v1beta1
  kind: CronJob
  metadata:
    name: sync-lichess
    namespace: sync-lichess
  spec:
    # every day at 1 am
    schedule: "0 1 * * *"
    concurrencyPolicy: Replace
    jobTemplate:
      spec:
        template:
          spec:
            volumes:
              - name: lichess-volume
                hostPath:
                  path: /mnt/data/pgns/
            containers:
              - name: sync-lichess
                image: docker.io/reedrichards/sync-lichess:70475835
                volumeMounts:
                  - mountPath: /mnt/data/pgns
                    name: lichess-volume
                envFrom:
                  - secretRef:
                      name: lichess-secrets
            restartPolicy: OnFailure
#+END_EXAMPLE
* get help on any kubectl command
 #+begin_src bash
 kubectl cp --help
 Copy files and directories to and from containers.
 Examples:
 # !!!Important Note!!!
 # Requires that the 'tar' binary is present in your container
 # image.  If 'tar' is not present, 'kubectl cp' will fail.

 # Copy /tmp/foo_dir local directory to /tmp/bar_dir in a remote pod in the default namespace
 kubectl cp /tmp/foo_dir <some-pod>:/tmp/bar_dir

 # Copy /tmp/foo local file to /tmp/bar in a remote pod in a specific container
 kubectl cp /tmp/foo <some-pod>:/tmp/bar -c <specific-container>

 # Copy /tmp/foo local file to /tmp/bar in a remote pod in namespace <some-namespace>
 kubectl cp /tmp/foo <some-namespace>/<some-pod>:/tmp/bar

 # Copy /tmp/foo from a remote pod to /tmp/bar locally
 kubectl cp <some-namespace>/<some-pod>:/tmp/foo /tmp/bar

 Options:
 -c, --container='': Container name. If omitted, the first container in the pod will be chosen

 Usage:
 kubectl cp <file-spec-src> <file-spec-dest> [options]

 Use "kubectl options" for a list of global command-line options (applies to all commands).
 #+end_src
* base64 encode ~.kube/config~
  #+BEGIN_SRC bash :results output
cat ~/.kube/config | base64 -w 0
  
  #+END_SRC

  #+RESULTS:

* Notes
  I have a lot of yaml in this doc, and I can generate the code with 
~org-babel-tangle~. It would be nice if I could include the the generated
files with my build, and serve them with the website. That way, I can 
~kubectl apply -f https://g.jjk.is/kubernetes/example.yml~. 
