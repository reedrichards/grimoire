/**
 * * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */
// require("prismjs/prism.js");
// require("prismjs/components/prism-python");
// require("prismjs/components/prism-php");
// require("prismjs/components/prism-typescript");
// require("prismjs/components/prism-go");
// require("prismjs/components/prism-bash");
// require("prismjs/components/prism-markdown");
// require("prismjs/components/prism-lisp");
// require("prismjs/components/prism-yaml");
// require("prismjs/components/prism-ruby");
// require("prismjs/components/prism-hcl");
// require("prismjs/components/prism-sql");
// require("prismjs/plugins/toolbar/prism-toolbar");
// require("prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard");
// require("prismjs/plugins/show-language/prism-show-language");
// require("prismjs/plugins/line-numbers/prism-line-numbers");
// require("prismjs/themes/prism-okaidia.css");

// ES6
function slugify(str) {
  // str = str.replace(/^\s+|\s+/g, ''); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to = "aaaaeeeeiiiioooouuuunc------";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-"); // collapse dashes

  return str;
}

const sectionMapFn = async section => {
  section.firstChild.id = slugify(section.firstChild.textContent);
  section.firstChild.addEventListener(
    "click",
    () => (window.location.hash = "#" + slugify(section.firstChild.textContent))
  );
};
export const onClientEntry = () => {
  window.onload = async () =>
    Array.from(document.getElementsByClassName("section")).map(sectionMapFn);
};
