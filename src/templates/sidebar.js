import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import toFileTree from 'to-file-tree'

const mapFilesToLI = (file, pathPrefix) => (
  <li>
    <a href={pathPrefix + '/' + file.split('.')[0]}>
      {
        file
          .split('/')
          .pop()
          .split('.')[0]
      }
    </a>
  </li>
)

const fileTreeToSideBar = (fileTree, pathPrefix) => (
  <ul className="sidebar">
    {fileTree.files.map(file => mapFilesToLI(file, pathPrefix))}
    {Object.keys(fileTree.directories).length > 0
      ? Object.keys(fileTree.directories).map(key => (
        <section>
          {key}
          <ul className="sidebar">
            {fileTreeToSideBar(fileTree.directories[key], pathPrefix)}
          </ul>
        </section>
      ))
      : () => { }}
  </ul>
)

export default () => (
  <StaticQuery
    query={graphql`
      query SidebarQuery {
        allFile {
          edges {
            node {
              relativePath
            }
          }
        }
        site {
          pathPrefix
        }
      }
    `}
    render={data => (
      <div style={{}}>
        {fileTreeToSideBar(
          toFileTree(
            data.allFile.edges.map(edge => edge.node.relativePath),
            true
          ),
          data.site.pathPrefix
        )}
      </div>
    )}
  />
)
