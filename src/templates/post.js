import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import TableOfContents from '../components/tableOfContents'
import Prism from 'prismjs'
import { parse } from 'node-html-parser'

import 'prismjs/components/prism-markup-templating'
import 'prismjs/components/prism-python'
import 'prismjs/components/prism-php'
import 'prismjs/components/prism-typescript'
import 'prismjs/components/prism-go'
import 'prismjs/components/prism-bash'
import 'prismjs/components/prism-markdown'
import 'prismjs/components/prism-lisp'
import 'prismjs/components/prism-hcl'
import 'prismjs/components/prism-sql'
import 'prismjs/components/prism-yaml'
import 'prismjs/components/prism-ruby'
import 'prismjs/components/prism-jsx'
import 'prismjs/components/prism-docker'
import 'prismjs/components/prism-apacheconf'
import 'prismjs/plugins/toolbar/prism-toolbar'
import 'prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard'
import 'prismjs/plugins/show-language/prism-show-language'

import './base16-eva.css'

const isSection = cnode =>
  typeof cnode !== 'undefined' &&
  typeof cnode.classNames !== 'undefined' &&
  cnode.tagName === 'div' &&
  cnode.classNames.includes('section')

const sectionText = section => section.firstChild.rawText

const toTableOfContents = section => (
  <TableOfContents text={sectionText(section)}>
    {section.childNodes.filter(isSection).map(toTableOfContents)}
  </TableOfContents>
)

export default class BlogPostTemplate extends React.Component {
  componentDidMount() {
    // some languages like lisp won't highlight unless this is here
    Prism.highlightAll();

  };

  render() {
    const post = this.props.data.orgContent
    const html = parse(post.html)
    const title = post.meta.title

    return (
      <Layout>
        <div id="content" style={{}}>
          <div style={{ padding: '10px' }}>
            <h1 style={{ fontSize: '3em' }}>
              {title === 'Untitled' ? '' : title}
            </h1>
          </div>
          <div>
            <div
              style={{
                padding: '.5rem .5rem',
                display: 'flex',
                justifyContent: 'center',
                borderRadius: '0.3rem',
              }}
            >
              <h2>Table of Contents</h2>
            </div>
            <div style={{ padding: '.5rem .5rem' }}>
              {html.childNodes.filter(isSection).map(toTableOfContents)}
            </div>
          </div>
          <div style={{ padding: '10px' }}>
            <div dangerouslySetInnerHTML={{ __html: post.html }} />
          </div>
        </div>
      </Layout>
    )
  }
}


export const pageQuery = graphql`
  query BlogPostQuery($id: String!) {
    orgContent(id: { eq: $id }) {
      html
      meta {
        title
      }
    }
  }
`
