import React from 'react'
import Layout from '../components/layout'

class BlogIndex extends React.Component {
  render() {
    return (
      <Layout>
        <div>
          <p>
            A <a href="https://en.wikipedia.org/wiki/Grimoire">Grimoire</a> is a
            book of spells, typically including instructions on how to create
            magical objects such as talismans and amulets. My grimoire is
            similar, it contains construction on how to create magical software
            and useful code snippets. It also includes more general notes on
            health, interesting quotes, or whatever else I feel like jotting
            down.
          </p>
          <p>
            This site serves more as a public facing web representation of my
            notes, and isn't really intended to be perfect in terms of
            aesthetics or presentation. This is somewhat inspired by{' '}
            <a href="https://www.gwern.net">gwern.net</a>. I try to include as
            much of my notes as possible here because there is only beinifit
            from having my information to be easily accessible, even if it is
            just for myself. There are a few posts on here that will evnetually
            become blog posts, and will make there way to the under construction{' '}
            <a href="blog.cacheconsiderations.com">blog</a>.
          </p>
        </div>
      </Layout>
    )
  }
}

export default BlogIndex
