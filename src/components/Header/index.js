import React from 'react'
import { Link } from 'gatsby'
import style from './_style.module.css'
import HamburgerMenu from 'react-hamburger-menu'
import Sidebar from '../../templates/sidebar'

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { sidebarOpen: false };
  }
  render() {
    return (
      <div>
        <div className={style.container}>
          <div
            style={{
              maxWidth: 960,
              padding: '1.45rem 1.0875rem',
            }}
          >
            <h1 style={{}}>
              <Link
                to="/"
                style={{
                  color: '#fdfcfb',
                  textDecoration: 'none',
                }}
              >
                Grimoire
                </Link>
            </h1>
          </div>
          <div
            style={{
              maxWidth: 960,
              padding: '1.45rem 1.0875rem',
            }}
          >
            <HamburgerMenu
              isOpen={this.state.open}
              menuClicked={() => this.setState({
                sidebarOpen: !this.state.sidebarOpen,
                open: !this.state.open
              })}
              strokeWidth={5}
              rotate={0}
              color='black'
              borderRadius={0}
              animationDuration={0.5}
            />
          </div>
        </div>
        {this.state.sidebarOpen ? <Sidebar /> : () => { }}
      </div>
    )
  }

}
