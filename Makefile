build-serve:
	yarn run build && python3 -m http.server 8000 --directory public/
dev:
	yarn run develop

pull-dotfiles:
	git clone  https://gitlab.com/reedrichards/dotfiles docs/dotfiles.bak
	mkdir -p docs/dotfiles
	mv docs/dotfiles.bak/*.org docs/dotfiles
	rm -rf docs/dotfiles.bak

conf-apacheconf:
	grep -rl "src conf\|SRC conf" docs  | xargs sed -i 's/\(src\) conf\|\(SRC\) conf/\1\2 apacheconf/'
