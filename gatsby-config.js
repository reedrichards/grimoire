module.exports = {
  pathPrefix: process.env.GRIMOIRE_PATH_PREFIX,
  siteMetadata: {
    title: "Grimoire"
  },
  plugins: [
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/docs/`
      }
    },
    {
      resolve: `gatsby-transformer-orga`,
      options: {
        // if you don't want to have server side prism code highlight
        // noHighlight: true
      }
    }
  ]
};
